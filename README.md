# Caldera UI

VueJS plugin for generating ui elements for WordPress plugin development.

This is a thin layer over [bootstrap-vue](https://bootstrap-vue.js.org/). By wrapping in custom elements we are black-boxing the dependency.


## Installation
 
 ```
npm install caldera-ui
```

## Usage
[See docs for library this is wrapping](https://bootstrap-vue.js.org/docs/). Replace `b` with 'cf-' and make sure component has been registered.


## License and attribution.

This is forked from [VuePack](https://github.com/egoist/vuepack)

This is copyright 2017-2112 CalderaWP.

This is licensed under the terms of the GNU GPL v2+, please share with your neighbor.