import { bForm, bInput, bInputGroup, bCheck, bCheckboxGroup, bRadio, bRadioGroup } from 'bootstrap-vue/lib/components'

const COMPONENTS = {
	'cf-form' : bForm,
	'cf-input' : bInput,
	'cf-input-group' : bInputGroup,
	'cf-check' : bCheck,
	'cf-check-group' : bCheckboxGroup,
	'cf-radio' : bRadio,
	'cf-radio-group' : bRadioGroup,
};


const CalderaUI = {
	install(Vue, options) {
		Vue.mixin({
			components: COMPONENTS
		});

	}
};

export default CalderaUI;